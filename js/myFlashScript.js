
    params = { AllowScriptAccess: 'always', AllowFullscreen: true };

    swfobject.embedSWF("http://cdn.livestream.com/chromelessPlayer/v20/playerapi.swf",
      "livestreamPlayer", "533", "300", "9.0.0", "expressInstall.swf", null, params);

	function switchChannel() {
		var channelInput = document.getElementById('channelInput');
		player.load(channelInput.value);
		player.setClipID(null);
		player.startPlayback();
	}
		
	startTime = 0;
	sliderStep = 0;
	clipDuration = 0;
	sliderUpdateTimer = null;
	
	function livestreamPlayerCallback(event) {
		eventDiv = document.getElementById('log');
		eventDiv.innerHTML = eventDiv.innerHTML + event + '<br/>';
		
		if (event == 'ready') {
            varInit();
            player = document.getElementById("livestreamPlayer");
            player.setDevKey('J2_3PsAB-l5aYHerRSIZ2lb5qRq4AaGFEIPho0JKMbiErqtNXOyw4r7v3SYED7YSghd9WalxeFUSSX71FayVw2ZmIW_69zEA4lukT6IadIs');
			//player.load('169demochannel');
            //player.load('proshowcase');
            player.load("proshowcase", "flv_7e12afb2-fae2-49e7-aef2-269febafbf10");
            player.showFullscreenButton(true);
            player.startPlayback();
	
			setInterval("updateViewerCount()", 1000);
		}
		else if (event == 'playbackEvent') {
			clipDuration = player.getDuration();
			slider1.set(0);
			sliderStep = 400 / clipDuration;
			if (!sliderUpdateTimer) {
				sliderUpdateTimer = setInterval("updateSlider()", 1000);			
			}
		}
		else if (event == 'disconnectionEvent') {
			clearInterval(sliderUpdateTimer);
		}
	}	
      
    function varInit(){
        

        volumeString = "Volume: ";
        eventDiv = document.getElementById('log');
    }
      
    function volumeUp(){
        
        player.volumeUp();
        volumeStatus = player.getVolume();
        eventDiv.innerHTML = eventDiv.innerHTML + volumeString + volumeStatus + '<br/>';
    }
	
	function updateSlider() {
		var time = Math.round(player.getTime());
		var duration = Math.round(player.getDuration());
		if (isNaN(time) || isNaN(duration) || isNaN(sliderStep)) {
			return;
		}
		document.getElementById('elapsed').style.width = time * sliderStep;		
		document.getElementById('time').innerHTML = time + ':' + duration;
	}
	
	function updateViewerCount() {
		div = document.getElementById('viewercount');
		div.innerHTML = player.getViewerCount() + ' viewers';
	}
	
	function seek(val) {
		if (val == 0) {
			return;
		}
		if (player) {
			newPos = val / 100 * player.getDuration();
			player.seek(newPos);
			sliderPos = val / 100 * 400;
			document.getElementById('elapsed').style.width = sliderPos;
			currentTime = newPos;
		}
	}
	
	swfobject.addLoadEvent(function() {
		slider1 = new Slider('slideContainer1', 'slideHandle1',
				{ onComplete: function(val){ seek(val) } } 
		)}
	);