jQuery(document).ready(function($) {
    
    var progressiveUrlData;
    var progressive_url_hdData;
          
    $("#URLForm").submit(function(event) {

        // Prevent the form from submitting via the browser.
        event.preventDefault();
        
        //console.log("click!");
        sendAjax();
    });
    
    /*function normalQuality(){
        
        $('#divVideo video source').attr('src', progressiveUrlData);
        $("#divVideo video")[0].load();
    }
    
    function highQuality(){
        
        $('#divVideo video source').attr('src', progressive_url_hd);
        $("#divVideo video")[0].load();
    }*/
    
    $("#normalButton").click(function(){
        
        $('#divVideo video source').attr('src', progressiveUrlData);
        $("#divVideo video")[0].load();
    });
    
    $("#highButton").click(function(){
        
        $('#divVideo video source').attr('src', progressive_url_hdData);
        $("#divVideo video")[0].load();
    });
    
    function sendAjax() {

        var sendData = {}
        sendData["formVideoAPI"] = $("#formVideoAPI").val();
        //console.log(sendData.formVideoAPI);
        
        //Using a proxy ONLY as temporal workaround for CORS, 
        //it must be removed from the code before production!
        
        var proxy = "https://jsonp.afeld.me/?callback=?&url=";
        var finalURL = proxy + sendData.formVideoAPI;

        $.getJSON(finalURL, function(data){

            progressiveUrlData = data["progressive_url"];
            progressive_url_hdData = data["progressive_url_hd"];
            
            if(progressiveUrlData){
                console.log(progressiveUrlData);
                
                $('#divVideo video source').attr('src', progressiveUrlData);
                $("#divVideo video")[0].load();
                
                $('#normalButton').prop("disabled",false);
            }
            if(progressive_url_hdData){
                
                console.log(progressive_url_hdData);
                
                $('#highButton').prop("disabled",false);
                }
            
        }); 
    } 
});