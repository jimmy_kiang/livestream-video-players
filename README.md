# Front End Video PLayer flash/html.

This project contains 2 examples of video players: 

1) Pure HTML5 video player (no flash plugin required).

2) Flash plugin from original.livestream.com 



## Running the application locally.

(optional): For local develpment and testing, I used the http-server package for node.js from: https://www.npmjs.com/package/http-server. This was necessary specially as a work around for the flash video player, as it avoided the error "please select channel" even when the channel information was already passed to the API.

### Running the HTML 5 Video player version.

1) To use the HTML version of the player open the file (htmlVideo.html) served from the http-server package. For example I used the following URL from the Google Chrome Browser:

   http://localhost:8080/htmlVideo.html

2) Paste or type an API endpoint from livestream, using the example from the coding test:

    http://api.new.livestream.com/accounts/11707815/events/4299357/stream_info
    
What happens internally is that the javascript code will make a GET request from the Endpoint and use the JSON response information to enable the user to switch between two video quality from the JSON properties: progressive_url and progressive_url_HD.

   (optional only for development): Cross Origin Site Requests problems will arise. Since the frontEnd local domain is trying to access a remote domain (api.new.livestream.com). To avoid this and only as a temporary work around, I used a proxy server to be able to get the JSONP response without errors. (This proxy aproach should be removed in a production environment. The webPage should be served directly from the production server).
   
3) Depending on the response from from the livestream REST Endpoint, the normal and/or the high quality version of the video will be enabled. As default, the normal quality will be set, so clicking on the high quality button (if its enabled) will inmediately switch to the higher quality version.


### Running the Flash Plugin Video player version.

1) To use the Flash Plugin Video player open the file (htmlVideo.html) served from the http-server package. For example I used the following URL from the Google Chrome Browser:

   http://localhost:8080/flashVideo.html

2) The channel and clip_ID are hardwired into the code and by default will load the channel: proshowcase and the clip_ID: flv_7e12afb2-fae2-49e7-aef2-269febafbf10. 

    player.load("proshowcase", "flv_7e12afb2-fae2-49e7-aef2-269febafbf10");

3) The buttons Play/Pause/Volume up/ Volume Down/ Switch channel. Will call their corresponding javascript functions and pass the information to the Flash APÏ.

4) The fullscreen switching mode can be toggled only from the Flash Plugin (once the video is running click directly on the fullscreen icon inside the flash app), so no javascript interaction is enabled for such mode.



